#!/bin/bash

cd /var/opt/aws-cloudwan-checker
git config --global --add safe.directory /var/opt/aws-cloudwan-checker
git fetch
git pull

python3 checker.py