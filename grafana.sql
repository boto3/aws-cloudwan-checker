SELECT src_ip, dst_ip, src_vpc, dst_vpc, src_az, dst_az, measure_name
		,max_by(time, time) AS latest_timestamp
		,max_by(measure_value::bigint, time) AS state
FROM $__database.$__table
WHERE time BETWEEN ago(1h) AND now()
		AND measure_name = 'tcp_check'
GROUP BY src_ip, dst_ip, src_vpc, dst_vpc, src_az, dst_az, measure_name
