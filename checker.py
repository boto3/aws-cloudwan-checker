#!/usr/bin/python3

import boto3
import requests
import os
import subprocess
import platform
import time
import socket

##############
DB_NAME = "cloud-wan-monitoring"
TBL_NAME = "table-icmp"
tcp_port = 22
REGION_LIST = ["eu-west-1", "us-east-1"]
##############

def get_imds_v2_token():
    try:
        # Obtenez le jeton d'authentification IMDSv2 à l'aide de Boto3
        iam_role_credentials = boto3.Session().get_credentials()
        response = requests.put(
            "http://169.254.169.254/latest/api/token",
            headers={"X-aws-ec2-metadata-token-ttl-seconds": "21600"},
            timeout=2,
            auth=(iam_role_credentials.token, "")
        )
        response.raise_for_status()

        return response.text

    except requests.RequestException as e:
        print(f"Erreur lors de la récupération du jeton IMDSv2 : {e}")
        return None


def get_instance_metadata_v2(key, imds_token):
    try:
        response = requests.get(
            f"http://169.254.169.254/latest/meta-data/{key}",
            headers={"X-aws-ec2-metadata-token": imds_token},
            timeout=2
        )
        response.raise_for_status()
        return response.text
    except requests.RequestException as e:
        print(f"Erreur lors de la récupération des métadonnées avec IMDSv2 : {e}")
        return None


def get_vpc_name(vpc_id, region):
    try:
        # Créez un client EC2
        ec2_client = boto3.client('ec2', region)

        # Utilisez describe_vpcs pour obtenir des informations sur le VPC
        response = ec2_client.describe_vpcs(
            VpcIds=[vpc_id]
        )

        # Vérifiez si le VPC a été trouvé
        if 'Vpcs' in response and response['Vpcs']:
            vpc_info = response['Vpcs'][0]
            vpc_name = 'N/A'  # Par défaut, le nom est défini sur "N/A" s'il n'est pas trouvé

            # Recherchez le tag 'Name' dans les tags du VPC pour obtenir le nom
            for tag in vpc_info.get('Tags', []):
                if tag['Key'] == 'Name':
                    vpc_name = tag['Value']
                    break

            return vpc_name
        else:
            return None

    except Exception as e:
        print(f"Une erreur s'est produite : {e}")
        return None


def ping_ok(Host) -> bool:
    try:
        subprocess.check_output(
            "ping -W 1 -c 1 {}".format(Host), shell=True
        )
    except Exception:
        return 1

    return 0
    
def check_server(address, port):

    # Create a TCP socket
    timeout = 2 #timeout in seconds
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(timeout)
    print ("Attempting to connect to %s on port %s" % (address, port))
    try:
        s.connect((address, port))
        print ("Connection to {} on port {} SUCCEED \n".format(address, port))
        return 0
    except socket.error as error:
        print ("Connection to {} on port {} FAILED \n".format(address, port))
        return 1
    
    
###############


imds_token = get_imds_v2_token()
#print(imds_token)
this_instance_id = get_instance_metadata_v2("instance-id", imds_token)
this_instance_vpc_id = ""


##############




custom_filter = [
    {'Name':'tag:Monitor', 'Values': ['True']},
    {'Name':'instance-state-name', 'Values': ['running']}
]

while True:
    time.sleep(180)
    CURRENT_TIME = str(int(time.time() * 1000))
    
    instances_list = []
    
    for region in REGION_LIST:
    	client = boto3.client('ec2', region)
    	response = client.describe_instances(Filters=custom_filter)
    	#print(response)
    
    	for reservation in response['Reservations']:
    	    for instance in reservation['Instances']:
    	        #print (instance['PrivateIpAddress'])
    	        #print (instance['Tags'])
    	        #print (instance['VpcId'])
    	        if instance['InstanceId'] != this_instance_id :
    	            my_dict = {}
    	            my_dict["dst_ip"] = instance['PrivateIpAddress']
    	            my_dict["dst_az"] = instance['Placement']['AvailabilityZone']
    	            my_dict["dst_vpc"] = get_vpc_name(instance['VpcId'], region)
    	            instances_list.append(my_dict)
    	            print(instances_list)
    	        else:
    	            this_instance_vpc_id = instance['VpcId']
    	            this_instance_az = instance['Placement']['AvailabilityZone']
    	            this_instance_ip = instance["PrivateIpAddress"]
    	            this_instance_vpc_name = get_vpc_name(this_instance_vpc_id, region)
    	            #print(this_instance_vpc_id)
            
    print(instances_list)
    
    # If no running instances (except self)
    if not instances_list:
        print("no running instances except myself !!!")
        continue
        # start the discovery process again

    
    
    #print(this_instance_id)
    #print(this_instance_vpc_name)
    #print(this_instance_ip)
    #print("#########")
    
    for target in instances_list:
       target['src_ip'] = this_instance_ip
       target['src_vpc'] = this_instance_vpc_name
       target['src_az'] = this_instance_az
       #target['icmp-check'] = ping_ok(target['dst_ip'])
       
       
    print(instances_list)
    print("\n")
    
    client = boto3.client('timestream-write', region_name='eu-west-1')
    records = []
    
    for target in instances_list:

        tcp_result = check_server(target['dst_ip'], tcp_port)
        dimension = [ {'Name': 'src_vpc', 'Value': target['src_vpc']}, {'Name': 'dst_vpc', 'Value': target['dst_vpc']}, {'Name': 'src_ip', 'Value': target['src_ip']}, {'Name': 'dst_ip', 'Value': target['dst_ip']}, {'Name': 'dst_az', 'Value': target['dst_az']}, {'Name': 'src_az', 'Value': target['src_az']}  ]
        record = { 'Time': CURRENT_TIME, 'Dimensions': dimension, 'MeasureName': 'tcp_check', 'MeasureValue': str(tcp_result),'MeasureValueType': 'BIGINT'}
        records.append(record)
    
    
    print(records)
    print("\n")
    response = client.write_records(DatabaseName=DB_NAME,TableName=TBL_NAME,Records=records)
    print(response)
    